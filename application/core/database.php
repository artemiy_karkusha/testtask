<?php

class Database {
    public $db;

    public function __construct($host, $db, $user, $pass, $charset = 'utf8'){
        try {
            $opt = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_PERSISTENT => true
            ];
            $this->db = new PDO("mysql:host=$host;dbname=$db", $user, $pass,$opt);
            $this->db->exec("set names $charset");
        }
        catch(PDOException $e) {
            throw $e;
        }
        return $this->db;
    }
    public function getAllUser($login){
        $query = $this->db->prepare('SELECT id, login, password FROM user_auth WHERE login = :login');
        $resultQuery = $query->execute([':login' => $login]);
        if (!$resultQuery){
            return FALSE;
        }
        $resultQuery = $query->fetchAll();
        return $resultQuery = $resultQuery[0];
    }


}