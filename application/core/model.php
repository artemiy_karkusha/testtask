<?php


class Model
{
    public function auth($login, $pass){
        $resSession = $this->authSession();

        if (!$resSession['Result']) {
            $pdo = new Database(HOST, DB, USER, PASS);
            $resultQuery = $pdo->getAllUser($login);
            if (!$resultQuery) {
                return [
                    'Result' => FALSE,
                    'Text' => 'Login not found'
                ];
            }
            if ($resultQuery['password'] != $pass) {
                return [
                    'Result' => FALSE,
                    'Text' => 'Password does not match'
                ];
            }
            $_SESSION['id'] = $resultQuery['id'];
            $_SESSION['login'] = $resultQuery['login'];
            $_SESSION['password'] = $resultQuery['password'];
        }
        return [
            'Result' => TRUE,
            'Text' => 'Coincidence'
        ];
    }
    public function authSession(){
        if(!isset($_SESSION)){
            return FALSE;
        }
        $pdo =  new Database(HOST,DB,USER,PASS);
        $resultQuery = $pdo->getAllUser($_SESSION['login']);
        if (!$resultQuery){
            return [
                'Result' => FALSE,
                'Text'=>'Login not found'
            ];
        }

        if ($_SESSION['password'] != $resultQuery['password']){
            return [
                'Result' => FALSE,
                'Text' =>'Password does not match'
            ];
        }
        return [
            'Result' => TRUE,
            'Text' => 'Session data == Db data'
        ];
    }
}

?>