<body class="sidebar-mini fixed">
<div class="wrapper">
    <!-- Navbar-->
    <header class="main-header hidden-print"><a class="logo" href="/admin">Admin</a>
        <nav class="navbar navbar-static-top">
            <!--<a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <li class="dropdown notification-menu"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell-o fa-lg"></i></a>
                        <ul class="dropdown-menu">
                            <li class="not-head">You have 4 new notifications.</li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block">2min ago</span></div></a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block">2min ago</span></div></a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block">2min ago</span></div></a></li>
                            <li class="not-footer"><a href="#">See all notifications.</a></li>
                        </ul>
                    </li>
                </ul>
            </div>-->
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image"><img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image"></div>
                <div class="pull-left info">
                    <p class="designation">
                        Admin</p>
                </div>
            </div>
            <!-- Sidebar Menu-->
        </section>
    </aside>
    <div class="content-wrapper">
        <div class="page-title">
            <div>
                <h1>Main</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><a href="/admin">Main</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="card">
                <form method="post" action="/admin">
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="" required>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="mail">E-mail</label>
                            <input type="text" class="form-control" id="mail" name="mail" placeholder="E-mail" value="" required>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary mt-20" type="submit" name="add">Add</button>
                        </div>
                    </div>
                    <?if($data['resultAdd'] === true){?>
                        <div class="alert alert-success mt-10" role="alert">
                            Подписка успешно создана
                        </div>
                    <?}?>
                    <?if($data['resultAdd'] === 'coincidence'):?>
                        <div class="alert alert-danger mt-10" role="alert">
                            Подписка уже существует
                        </div>
                    <?endif;?>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="card">
                <?if($data['resultUpdate'] === true){?>
                    <div class="alert alert-success mt-10" role="alert">
                        Подписка успешно обновлена
                    </div>
                <?}?>
                <?if($data['resultDelete'] === true){?>
                    <div class="alert alert-success mt-10" role="alert">
                        Подписка успешно удалена
                    </div>
                <?}?>
            <?php echo $data['table'];?>
            </div>
        </div>
    </div>

</div>
