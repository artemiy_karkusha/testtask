<?php if($data['verify']['result']==true):?>
<div id="welcome">
    <h1>Привіт,<?php echo $data['verify']["username"];?>!</h1>
    <?php if($data['result'] == true):?>
        <div class="subscribe">Вы получили доступ к странице</div>
    <?endif;?>
    <?php if(!empty($data['signin']['token'])):?>
        <p>Вам открыт доступ по ссылке <a><?=$data['signin']['token']?></a></p>
    <?endif;?>
    <p><a href="/logout">Отписатся</a></p>
</div>
<?endif;?>
<?if($data['verify']['result']==false):?>
    <div class="alert alert-danger" role="alert">
        У вас нету доступа к этой странице
    </div>
<?endif;?>
