<?php

class model_admin extends Model{
    private function getUser(){
        $connect = new Database(HOST, DB, USER, PASS);
        $statement = $connect->db->prepare('SELECT * FROM user');
        $statement->bindParam(':token', $token);
        $statement->execute();
        return $data = $statement->fetchAll();
    }
    public function genTable(){
        $body ='
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col" class="manifest-table-title-blue">#</th>
                            <th scope="col" class="manifest-table-title-blue">Username</th>
                            <th scope="col" class="manifest-table-title-blue">E-mail</th>
                            <th scope="col" class="manifest-table-title-blue">Action</th>
                        </tr>
                        </thead>
                        <tbody>';
        $data = $this->getUser();
        /*echo '<pre>';
        print_r($data);
        exit;*/
        for ($j = 0; $j < count($data); $j++) {
            $body .= '
                        <tr>
                            <th scope="row" class="manifest-table-blue">'.($j+1).'</th>
                            <form action="/admin" method="post">
                            <td class="manifest-table-blue"><input class="form-control" type="text" name="username" value="'.$data[$j]['username'].'"></td>
                            <td class="manifest-table-blue"><input class="form-control" type="text" name="mail" value="'.$data[$j]['mail'].'"</td>
                            <td class="manifest-table-blue">                        
                            <input class="btn btn-info"  type="submit" value="Delete" name="delete">
                            <input class="btn btn-info" type="submit" value="Update" name="update">
                            <input type="hidden" name="id" value="'.$data[$j]['id'].'">
                            </form>
                            <br>
                        </tr>';
        }
        $body.='</tbody>
                    </table>                   
         ';
        return $body;
    }

    public function deleteSub($id){
        $connect = new Database(HOST, DB, USER, PASS);
        try {
            $data = [
                'id' => $id,
            ];
            $sql = "DELETE FROM user WHERE id =  :id";
            $statement = $connect->db->prepare($sql);
            $statement->execute($data);
            return TRUE;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    public function updateSub($array){
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $sql = "UPDATE user SET username = :username,
                                mail = :mail
                    WHERE id = :id";
            $statement = $connect->db->prepare($sql);

            $statement->bindParam(':id', $array['id']);
            $statement->bindParam(':username', $array['username']);
            $statement->bindParam(':mail', $array['mail'] );
            $statement->execute();
            return TRUE;
        }catch (Exception $e){
            return $e->getMessage();
        }
    }

    private function generateStr(){
        $str = md5(uniqid(rand(), true));
        return $str;
    }

    private function dataTime(){
        date_default_timezone_set('Europe/Kiev');
        return date("Y-m-d H:i:s");
    }

    public function addSub($array){
        $connect = new Database(HOST, DB, USER, PASS);
        try {
            $statement = $connect->db->prepare('INSERT INTO user(username, mail, time_subs, token) VALUES (:username, :mail, :time_subs, :token)');

            $statement->bindParam(':username', $array['username']);
            $statement->bindParam(':mail', $array['mail']);
            $time_subs = $this->dataTime();
            $statement->bindParam(':time_subs', $time_subs);
            $token = $this->generateStr();
            $statement->bindParam(':token', $token);
            $statement->execute();

            return TRUE;
        }catch (Exception $e){
            return $e->getMessage();
        }

    }

    public function verifyCoincidence($mail){
        $connect = new Database(HOST, DB, USER, PASS);
        $statement = $connect->db->prepare('SELECT * FROM user WHERE mail = :mail');
        $statement->bindParam(':mail', $mail);
        $statement->execute();
        $data = $statement->fetchAll();
        return $data;
    }
}