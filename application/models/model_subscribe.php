<?php 

class model_subscribe extends Model
{
    private function generateStr(){
        $str = md5(uniqid(rand(), true));
        return $str;
    }
    private function dataTime(){
        date_default_timezone_set('Europe/Kiev');
        return date("Y-m-d H:i:s");
    }
    public function verifyCoincidence($mail){
        $connect = new Database(HOST, DB, USER, PASS);
        $statement = $connect->db->prepare('SELECT * FROM user WHERE mail = :mail');
        $statement->bindParam(':mail', $mail);
        $statement->execute();
        $data = $statement->fetchAll();
        return $data;
    }
    public function signIn($data){
        try {
            $connect = new Database(HOST, DB, USER, PASS);
            $statement = $connect->db->prepare('INSERT INTO user(username, mail, time_subs, token) VALUES (:username, :mail, :time_subs, :token)');
            $statement->bindParam(':username', $data['username']);
            $statement->bindParam(':mail', $data['mail']);
            $time_subs = $this->dataTime();
            $statement->bindParam(':time_subs', $time_subs);
            $token = $this->generateStr();
            $statement->bindParam(':token', $token);
            $statement->execute();
            setcookie('token', $token, time() + 60 * 60 * 24 * 30);
        }catch(Exception $e){
            throw $e;
        }
        return [
            'result' => true,
            'token' => $_SERVER['HTTP_REFERER'] .'subscribe?token=' . $token,
        ];
    }
    public function verify($token){
        $connect = new Database(HOST, DB, USER, PASS);
        $statement = $connect->db->prepare('SELECT * FROM user WHERE token = :token');
        $statement->bindParam(':token', $token);
        $statement->execute();
        $data = $statement->fetchAll();
        $data = $data[0];
        if($_COOKIE['token']==$data['token']){
            return [
                'result' => true,
                'username' => $data['username']
            ];
        }else{
            return false;
        }
    }

}