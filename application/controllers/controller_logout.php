<?php

class Controller_logout extends Controller
{

    function __construct()
    {
        $this->model = new model_logout();
        $this->view = new View();
    }


    function action_index()
    {
        $this->view->generate('main_view.php', 'template_view.php');
    }
}