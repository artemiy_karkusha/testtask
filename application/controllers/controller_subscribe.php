<?php

class Controller_subscribe extends Controller
{

    function __construct()
    {
        $this->model = new model_subscribe();
        $this->view = new View();
    }

    function action_index()
    {
        $data = null;
        if ($_POST) {
            $array = $this->model->verifyCoincidence($_POST['mail']);
            if (empty($array)) {
                $data['signin'] = $this->model->signIn($_POST);
                $data['verify']['result'] = true;
            } else {
                $data['mailCoincidence'] = '';
            }
        }
        if($_GET){
            if(isset($_GET['token'])){
                $data['verify'] = $this->model->verify($_GET['token']);
            }
        }
        $this->view->generate('subscribe_view.php', 'template_view.php',$data);
    }
}