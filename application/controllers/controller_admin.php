<?php

class Controller_admin extends Controller
{

    function __construct()
    {
        $this->model = new model_admin();
        $this->view = new View();
    }

    function action_index()
    {
        $this->auth();
        if($_POST){
            /*echo "<pre>";
            print_r($_POST);
            exit;*/
            if(isset($_POST['delete'])){
                $data['resultDelete'] = $this->model->deleteSub($_POST['id']);
            }
            if(isset($_POST['update'])){
                $data['resultUpdate'] = $this->model->updateSub($_POST);
            }
            if(isset($_POST['add'])){
                $resultVer = $this->model->verifyCoincidence($_POST['mail']);
                if(empty($resultVer)){
                    $data['resultAdd'] = $this->model->addSub($_POST);
                }else{
                    $data['resultAdd'] = 'coincidence';
                }
            }
        }
        $data['table'] = $this->model->genTable();
        $this->view->generate('admin_view.php', 'template_admin_view.php',$data);
    }
}